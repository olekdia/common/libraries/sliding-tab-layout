object Versions {
    const val kotlin = "1.7.20"
    const val junit = "4.13"
    const val robolectric = "4.3.1"

    object olekdia {
        const val common = "0.6.1"
        const val common_android = "3.7.4"
    }

    object sdk {
        const val min = 21
        const val target = 32
        const val compile = 32
    }
    const val buildTools = "33.0.0"

    object androidx {
        const val annotations = "1.5.0"
        const val core = "1.8.0"
        const val appcompat = "1.5.1"
        const val viewpager = "1.0.0"
        const val material = "1.7.0"
        const val fragment = "1.5.4"

        const val test_core = "1.4.0"
        const val test_runner = "1.4.0"
        const val test_rules = "1.4.0"
    }

    const val android_gradle = "7.3.1"
}


object Libs {
    val junit = "junit:junit:${Versions.junit}"
    val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"

    object olekdia {
        private val common_prefix = "com.olekdia:multiplatform-common"
        val common = "$common_prefix:${Versions.olekdia.common}"
        val common_jvm = "$common_prefix-jvm:${Versions.olekdia.common}"

        val common_android = "com.olekdia:android-common:${Versions.olekdia.common_android}"
    }

    object androidx {
        val annotations = "androidx.annotation:annotation:${Versions.androidx.annotations}"
        val core = "androidx.core:core:${Versions.androidx.core}"
        val appcompat = "androidx.appcompat:appcompat:${Versions.androidx.appcompat}"
        val viewpager = "androidx.viewpager:viewpager:${Versions.androidx.viewpager}"
        val material = "com.google.android.material:material:${Versions.androidx.material}"
        val fragment = "androidx.fragment:fragment:${Versions.androidx.fragment}"

        val test_core = "androidx.test:core:${Versions.androidx.test_core}"
        val test_core_ktx = "androidx.test:core-ktx:${Versions.androidx.test_core}"
        val test_runner = "androidx.test:runner:${Versions.androidx.test_runner}"
        val test_rules = "androidx.test:rules:${Versions.androidx.test_rules}"
    }

    object plugin {
        val kotlin_gradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        val android_gradle = "com.android.tools.build:gradle:${Versions.android_gradle}"
    }
}