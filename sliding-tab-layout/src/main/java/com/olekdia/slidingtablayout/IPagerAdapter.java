package com.olekdia.slidingtablayout;

import androidx.viewpager.widget.ViewPager;

public interface IPagerAdapter {

    int getCount();

    int getPageIconRes(int position);

    CharSequence getPageTitle(int position);

    boolean isVisibleTitles();

    void setPageChangeListener(ViewPager.OnPageChangeListener l);
}
