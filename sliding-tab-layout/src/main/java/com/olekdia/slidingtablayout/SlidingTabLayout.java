/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.slidingtablayout;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olekdia.androidcommon.extensions.ResExt;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.viewpager.widget.ViewPager;

import static com.olekdia.androidcommon.Constants.NO_RESOURCE;

/**
 * To be used with ViewPager to provide a tab indicator component which give constant feedback as to
 * the user's scroll progress.
 * <p>
 * To use the component, simply add it to your view hierarchy. Then in your
 * {@link android.app.Activity} or {android.support.v4.Fragment} call
 * {@link #setupWithViewPager(ViewPager, int)} providing it the ViewPager this layout is being used for.
 * <p>
 * The colors can be customized in two ways. The first and simplest is to provide an array of colors
 * via {@link #setSelectedIndicatorColors(int...)}. The
 * alternative is via the {@link TabColorizer} interface which provides you complete control over
 * which color is used for any individual position.
 * <p>
 * The views used as tabs can be customized by calling {@link #setCustomTabView(int)},
 * providing the layout ID of your custom layout.
 */
public class SlidingTabLayout extends HorizontalScrollView implements View.OnClickListener {

    /**
     * Allows complete control over the colors drawn in the tab layout. Set with
     * {@link #setCustomTabColorizer(TabColorizer)}.
     */
    public interface TabColorizer {
        /**
         * @return return the color of the indicator used when {@code position} is selected.
         */
        int getIndicatorColor(int position);
    }

    private final int mTitleOffset;
    @LayoutRes
    private int mTabViewLayoutId;
    private boolean mDistributeEvenly;
    private ViewPager mViewPager;
    private ViewPager.OnPageChangeListener mViewPagerPageChangeListener;
    private TabSelectionInterceptor mTabSelectionInterceptor;
    private final SlidingTabStrip mTabStrip;
    private ColorStateList mTintList;

    public SlidingTabLayout(Context context) {
        this(context, null);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setHorizontalScrollBarEnabled(false);

        // Make sure that the Tab Strips fills this View
        setFillViewport(true);
        mTitleOffset = ResExt.getDimensionPixelSize(context, R.dimen.stl_title_offset);
        mTabStrip = new SlidingTabStrip(context);
        addView(mTabStrip, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    public final void setTintList(final ColorStateList tint) {
        mTintList = tint;
    }

    /**
     * Set the custom {@link TabColorizer} to be used.
     * <p/>
     * If you only require simple custmisation then you can use
     * {@link #setSelectedIndicatorColors(int...)} to achieve
     * similar effects.
     */
    public final void setCustomTabColorizer(final TabColorizer tabColorizer) {
        mTabStrip.setCustomTabColorizer(tabColorizer);
    }

    public final void setDistributeEvenly(final boolean distributeEvenly) {
        mDistributeEvenly = distributeEvenly;
    }

    /**
     * Sets the colors to be used for indicating the selected tab. These colors are treated as a
     * circular array. Providing one color will mean that all tabs are indicated with the same color.
     */
    public final void setSelectedIndicatorColors(final int... colors) {
        mTabStrip.setSelectedIndicatorColors(colors);
    }

    /**
     * Set the {@link ViewPager.OnPageChangeListener}. When using {@link SlidingTabLayout} you are
     * required to set any {@link ViewPager.OnPageChangeListener} through this method. This is so
     * that the layout can update it's scroll position correctly.
     *
     * @see ViewPager#setOnPageChangeListener(ViewPager.OnPageChangeListener)
     */
    public final void setOnPageChangeListener(final ViewPager.OnPageChangeListener listener) {
        mViewPagerPageChangeListener = listener;
    }

    public final void setTabSelectionInterceptor(final TabSelectionInterceptor interceptor) {
        mTabSelectionInterceptor = interceptor;
    }

    /**
     * Set the custom layout to be inflated for the tab views
     *
     * @param layoutResId Layout id to be inflated
     */
    public final void setCustomTabView(@LayoutRes final int layoutResId) {
        mTabViewLayoutId = layoutResId;
    }

    /**
     * Sets the associated view pager. Note that the assumption here is that the pager content
     * (number of tabs and tab titles) does not change after this call has been made.
     */
    public final void setupWithViewPager(@Nullable final ViewPager viewPager, @LayoutRes final int layoutRes) {
        mTabViewLayoutId = layoutRes;
        mTabStrip.removeAllViews();
        mViewPager = viewPager;
        if (viewPager != null) {
            viewPager.addOnPageChangeListener(new InternalViewPagerListener());
            populateTabStrip();
        }
    }

    private void populateTabStrip() {
        final IPagerAdapter adapter = (IPagerAdapter) mViewPager.getAdapter();
        final Context ctx = getContext();
        final LayoutInflater inflater = LayoutInflater.from(ctx);

        CharSequence title;

        for (int i = 0; i < adapter.getCount(); i++) {
            final TextView tabView = (TextView) inflater.inflate(mTabViewLayoutId, this, false);
            tabView.setClickable(true);

            final int tabIconRes = adapter.getPageIconRes(i);
            if (tabIconRes != NO_RESOURCE) {
                final Drawable tabDrw = DrawableCompat.wrap(ContextCompat.getDrawable(ctx, tabIconRes));
                DrawableCompat.setTintList(tabDrw, mTintList);
                tabView.setCompoundDrawablesWithIntrinsicBounds(tabDrw, null, null, null);
            }

            tabView.setOnClickListener(this);

            title = adapter.getPageTitle(i);

            tabView.setTextColor(mTintList);
            if (adapter.isVisibleTitles()) tabView.setText(title);
            tabView.setContentDescription(title);

            mTabStrip.addView(tabView);
            if (mDistributeEvenly) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                lp.width = 0;
                lp.weight = 1;
                lp.gravity = Gravity.CENTER;
            }
            if (i == mViewPager.getCurrentItem()) {
                tabView.setSelected(true);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mViewPager != null) scrollToTab(mViewPager.getCurrentItem(), 0);
    }

    public final void scrollToTab(final int tabIndex, final int positionOffset) {
        final int tabStripChildCount = mTabStrip.getChildCount();
        if (tabStripChildCount == 0
            || tabIndex < 0
            || tabIndex >= tabStripChildCount
        ) {
            return;
        }
        final View selectedChild = mTabStrip.getChildAt(tabIndex);
        if (selectedChild != null) {
            int targetScrollX = selectedChild.getLeft() + positionOffset;
            if (tabIndex > 0 || positionOffset > 0) {
                targetScrollX -= mTitleOffset;
            }
            scrollTo(targetScrollX, 0);
        }
    }

    @Override
    public void onClick(View v) {
        if (mTabSelectionInterceptor != null
            && mTabSelectionInterceptor.shouldInterceptTabSelection(v)
        ) {
            return;
        }

        for (int i = 0; i < mTabStrip.getChildCount(); i++) {
            if (v == mTabStrip.getChildAt(i)) {
                mViewPager.setCurrentItem(i);
                return;
            }
        }
    }

    private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {
        private int mScrollState;

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            int tabStripChildCount = mTabStrip.getChildCount();
            if ((tabStripChildCount == 0)
                || (position < 0)
                || (position >= tabStripChildCount)
            ) {
                return;
            }
            mTabStrip.onViewPagerPageChanged(position, positionOffset);
            final View selectedTitle = mTabStrip.getChildAt(position);
            int extraOffset = (selectedTitle != null)
                ? (int) (positionOffset * selectedTitle.getWidth())
                : 0;
            scrollToTab(position, extraOffset);
            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageScrolled(
                    position,
                    positionOffset,
                    positionOffsetPixels
                );
            }
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            mScrollState = state;
            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageScrollStateChanged(state);
            }
        }

        @Override
        public void onPageSelected(final int position) {
            if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                mTabStrip.onViewPagerPageChanged(position, 0f);
                scrollToTab(position, 0);
            }
            for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                mTabStrip.getChildAt(i).setSelected(position == i);
            }
            if (mViewPagerPageChangeListener != null) {
                mViewPagerPageChangeListener.onPageSelected(position);
            }
        }
    }

    public interface TabSelectionInterceptor {
        boolean shouldInterceptTabSelection(View view);
    }
}