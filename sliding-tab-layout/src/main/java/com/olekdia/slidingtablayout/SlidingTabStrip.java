/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olekdia.slidingtablayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.olekdia.common.extensions.ColorExt;
import com.olekdia.androidcommon.extensions.ResExt;

class SlidingTabStrip extends LinearLayout {

    private static final byte DEFAULT_BOTTOM_BORDER_COLOR_ALPHA = 0x26;
    private static final int DEFAULT_SELECTED_INDICATOR_COLOR = 0xFFFFFFFF;
    private final int mBottomBorderThickness;
    private final Paint mBottomBorderPaint;
    private final int mSelectedIndicatorThickness;
    private final Paint mSelectedIndicatorPaint;
    private final int mDefaultBottomBorderColor;
    private int mSelectedPosition;
    private float mSelectionOffset;
    private SlidingTabLayout.TabColorizer mCustomTabColorizer;
    private final SimpleTabColorizer mDefaultTabColorizer;

    SlidingTabStrip(Context context) {
        this(context, null);
    }

    SlidingTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);

        final int themeForegroundColor = ResExt.resolveColor(
            context,
            android.R.attr.colorForeground
        );
        mDefaultBottomBorderColor = ColorExt.withAlpha(
            themeForegroundColor,
            DEFAULT_BOTTOM_BORDER_COLOR_ALPHA
        );
        mDefaultTabColorizer = new SimpleTabColorizer();
        mDefaultTabColorizer.setIndicatorColors(DEFAULT_SELECTED_INDICATOR_COLOR);
        mBottomBorderThickness = ResExt.getDimensionPixelSize(
            context,
            R.dimen.stl_unselected_indicator_thickness
        );
        mBottomBorderPaint = new Paint();
        mBottomBorderPaint.setColor(mDefaultBottomBorderColor);
        mSelectedIndicatorThickness = ResExt.getDimensionPixelSize(
            context,
            R.dimen.stl_selected_indicator_thickness
        );
        mSelectedIndicatorPaint = new Paint();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setLayoutDirection(LAYOUT_DIRECTION_LTR);
        }
    }

    final void setCustomTabColorizer(final SlidingTabLayout.TabColorizer customTabColorizer) {
        mCustomTabColorizer = customTabColorizer;
        invalidate();
    }

    final void setSelectedIndicatorColors(final int... colors) {
        mCustomTabColorizer = null;
        mDefaultTabColorizer.setIndicatorColors(colors);
        invalidate();
    }

    final void onViewPagerPageChanged(final int position, final float positionOffset) {
        mSelectedPosition = position;
        mSelectionOffset = positionOffset;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int height = getHeight();
        final int childCount = getChildCount();
        final SlidingTabLayout.TabColorizer tabColorizer = mCustomTabColorizer != null
            ? mCustomTabColorizer
            : mDefaultTabColorizer;
        if (childCount > 0) {
            final View selectedTitle = getChildAt(mSelectedPosition);
            int left = selectedTitle.getLeft();
            int right = selectedTitle.getRight();
            int color = tabColorizer.getIndicatorColor(mSelectedPosition);
            if (mSelectionOffset > 0F && mSelectedPosition < (getChildCount() - 1)) {
                final int nextColor = tabColorizer.getIndicatorColor(mSelectedPosition + 1);
                if (color != nextColor) {
                    color = blendColors(nextColor, color, mSelectionOffset);
                }
                final View nextTitle = getChildAt(mSelectedPosition + 1);
                left = (int) (mSelectionOffset * nextTitle.getLeft() +
                    (1.0F - mSelectionOffset) * left);
                right = (int) (mSelectionOffset * nextTitle.getRight() +
                    (1.0F - mSelectionOffset) * right);
            }
            mSelectedIndicatorPaint.setColor(color);
            canvas.drawRect(left, height - mSelectedIndicatorThickness, right,
                            height, mSelectedIndicatorPaint);
        }
        canvas.drawRect(0, height - mBottomBorderThickness, getWidth(), height, mBottomBorderPaint);
    }

    /**
     * Blend {@code color1} and {@code color2} using the given ratio.
     *
     * @param ratio of which to blend. 1.0 will return {@code color1}, 0.5 will give an even blend,
     *              0.0 will return {@code color2}.
     */
    private static int blendColors(final int color1, final int color2, final float ratio) {
        final float inverseRation = 1F - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    private static class SimpleTabColorizer implements SlidingTabLayout.TabColorizer {
        private int[] mIndicatorColors;

        @Override
        public final int getIndicatorColor(final int position) {
            return mIndicatorColors[position % mIndicatorColors.length];
        }

        void setIndicatorColors(final int... colors) {
            mIndicatorColors = colors;
        }
    }
}