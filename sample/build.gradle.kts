plugins {
    id("com.android.application")
}

android {
    compileSdk = Versions.sdk.compile
    buildToolsVersion = Versions.buildTools
    namespace = "com.olekdia.sample"

    defaultConfig {
        minSdk = Versions.sdk.min
        targetSdk = Versions.sdk.target
        applicationId = "com.olekdia.sample"
        versionCode = 1
        versionName = "1.0.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false
        }
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(kotlin("stdlib", Versions.kotlin))
    implementation(project(":sliding-tab-layout"))
    implementation(Libs.olekdia.common_android)

    implementation(Libs.androidx.appcompat)
    implementation(Libs.androidx.material)
    implementation(Libs.androidx.fragment)
}